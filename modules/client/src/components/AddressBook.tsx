import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { AddressEditor, AddressTable, CsvTable } from "@valuemachine/react";
import {
  AddressBook,
  AddressBookJson,
  AddressCategories,
  AddressEntry,
  CsvFiles,
  StoreKeys,
} from "@valuemachine/types";
import { getLocalStore } from "@valuemachine/utils";
import { Guards } from "@degentax/adapters";
import React, { useEffect, useState } from "react";

const store = getLocalStore(localStorage);

const getEmptyEntry = (): AddressEntry => ({
  category: AddressCategories.Self,
  name: "",
  address: "",
  guard: Guards.Ethereum,
});

const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    margin: theme.spacing(1),
    maxWidth: "98%",
  },
  title: {
    margin: theme.spacing(2),
  },
}));

export const AddressBookManager = ({
  addressBook,
  csvFiles,
  setCsvFiles,
  setAddressBookJson,
}: {
  addressBook: AddressBook,
  csvFiles: CsvFiles,
  setCsvFiles: (val: CsvFiles) => void,
  setAddressBookJson: (val: AddressBookJson) => void,
}) => {
  const [allAddresses, setAllAddresses] = useState([] as string[]);
  const [newEntry, setNewEntry] = useState(getEmptyEntry());
  const classes = useStyles();

  if (typeof setCsvFiles === "function") console.log(`We are able to set csv files`);

  useEffect(() => {
    setAllAddresses(Object.values(addressBook.json).map(entry => entry.address));
  }, [addressBook]);

  const addNewAddress = (editedEntry: AddressEntry) => {
    console.log(`Updating entry: ${JSON.stringify(editedEntry)}`);
    const newAddressBook = { ...addressBook.json } as AddressBookJson; // new obj ensure rerender
    newAddressBook[editedEntry.address] = editedEntry;
    setAddressBookJson(newAddressBook);
    // Don't reset new entry fields when we modify an existing one
    if (!allAddresses.includes(editedEntry.address)) {
      setNewEntry(getEmptyEntry());
    }
    store.save(StoreKeys.AddressBook, newAddressBook);
  };

  return (
    <div className={classes.root}>

      <Typography variant="h4" className={classes.title}>
        Manage Address Book
      </Typography>

      <Divider />

      <Grid
        alignContent="center"
        alignItems="center"
        justify="center"
        container
        spacing={1}
        className={classes.root}
      >
        <Card className={classes.root}>
          <CardHeader title={"Add new Address"} />
          <AddressEditor
            entry={newEntry}
            setEntry={addNewAddress}
            addresses={allAddresses}
          />
        </Card>
      </Grid>

      {Object.keys(addressBook.json).length
        ? <AddressTable addressBook={addressBook} setAddressBookJson={setAddressBookJson} />
        : null
      }

      {csvFiles.length
        ? <CsvTable csvFiles={csvFiles}/>
        : null
      }

    </div>
  );
};
