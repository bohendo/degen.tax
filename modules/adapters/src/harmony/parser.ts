import {
  parseEvmTx,
} from "@valuemachine/transactions";
import {
  AddressBook,
  EvmMetadata,
  EvmParsers,
  EvmTransaction,
  Logger,
  Transaction
} from "@valuemachine/types";

export const parseHarmonyTx = (
  harmonyTx: EvmTransaction,
  harmonyMetadata: EvmMetadata,
  addressBook: AddressBook,
  logger: Logger,
  extraParsers = { insert: [], modify: [] } as EvmParsers,
): Transaction => parseEvmTx(
  harmonyTx,
  harmonyMetadata,
  addressBook,
  logger,
  [extraParsers],
);
