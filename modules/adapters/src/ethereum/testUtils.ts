import {
  Account,
  Bytes32,
  Logger,
  Transaction,
} from "@valuemachine/types";
import { getEthereumData } from "valuemachine";

import { env, getTestAddressBook, testStore } from "../testUtils";

import { ethParsers } from ".";

export * from "../testUtils";

export const parseEthTx = async ({
  hash,
  selfAddress,
  logger,
}: {
  hash: Bytes32;
  selfAddress: Account;
  logger?: Logger;
}): Promise<Transaction> => {
  const addressBook = getTestAddressBook(selfAddress);
  const ethData = getEthereumData({
    covalentKey: env.covalentKey,
    etherscanKey: env.etherscanKey,
    logger,
    store: testStore,
  });
  await ethData.syncTransaction(hash, env.etherscanKey);
  return ethData.getTransaction(hash, addressBook, ethParsers);
};
