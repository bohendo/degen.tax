import { harmonyAddresses, harmonyParser } from "./harmony";

export const ethAddresses = [
  ...harmonyAddresses,
];

export const ethParsers = {
  insert: [...harmonyParser.insert],
  modify: [...harmonyParser.modify],
};
