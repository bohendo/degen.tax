export const env = {
  covalentKey: process.env.DEGENTAX_COVALENT_KEY || "",
  etherscanKey: process.env.DEGENTAX_ETHERSCAN_KEY || "",
  ethProvider: process.env.DEGENTAX_ETH_PROVIDER || process.env.VM_ETH_PROVIDER || "",
  logLevel: process.env.DEGENTAX_LOG_LEVEL || "info",
  port: parseInt(process.env.DEGENTAX_PORT || "8080", 10),
};
